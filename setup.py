import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="metalcore",
    version="0.0.3",
    author="The metalcore authors",
    author_email="info@quick2space.org",
    description="Python library for interacting with the MCP25625 CAN transceiver/controller",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/Quick2space/mcp25625.py",
    entry_points = {
        'console_scripts': ['metalcore.picture_recv=metalcore.picture_recv:main',
            'metalcore.pic_recv_gs=metalcore.pic_recv_gs:main',
            'metalcore.picture_recv_compressed=metalcore.apps.picture_recv_compressed:main',
            'metalcore.picture_send=metalcore.apps.picture_send:main',
            'metalcore.picture_send_compressed=metalcore.apps.picture_send_compressed:main',
            'metalcore.picture_send_nocompress=metalcore.apps.picture_send_nocompress:main',
            'metalcore=metalcore.cli:main',
            ]
        },
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
