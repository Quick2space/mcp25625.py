import socket, re, io
#from PIL import Image
import tkinter as tk
import time


class Test(object):




	def DrawPixels(self, arr):
		last_x = arr[0]
		last_y = arr[1]
		chksum = arr[7]

		pixels = arr[2:7]
		assert len(pixels) == 5

		received_chksum = self.CalculateChecksum(arr[0:7])

		# Invalid data?
		if chksum != received_chksum:
			return False
		
		x = last_x
		y = last_y

		while len(pixels) > 0:
			grayShade = (int(pixels[0])) % 255

			color = "#%02x%02x%02x" % (grayShade, grayShade, grayShade)
			# print("x y", x, y)
			self.C.create_rectangle(y * self.yScaleFactor, 
					x * self.xScaleFactor,
					(y + 1) * self.yScaleFactor, 
					(x + 1) * self.xScaleFactor, 
					fill=color)

			self.pixelsDrawn += 1 

			pixels = pixels[1:]
			y += 1
			if (y >= self.yd):
				y = 0
				x += 1
				if (x > self.xd):
					x = 0

		self.next_x_updated = x
		self.next_y_updated = y

		# Display some partial progress
		self.C.pack()
		self.top.update()

		# TODO - add threading and continuous update
		# if self.pixelsToBeDisplayedTest <= self.pixelsDrawn:
		#    self.top.mainloop()

		return True



	def __init__(self):
		pass





	def CalculateChecksum(self, data):
		s = 0
		for d in data:
			s = 7 * s + int(d % 255) * 11
			s = s % 255
		# print(data, " ---> ", s)
		return s

		

	def ProcessCAN(self):


		self.datacount = 0
		self.packetcount = 0
		self.firstPacket = True
		self.buildingData = False
		self.readyToEnd = False
		self.alldata = []


		self.xd = 96
		self.yd = 64

		# 5 pixels width 
		self.xScaleFactor = 6
		self.yScaleFactor = 5

		self.lastData = 0

		self.top = tk.Tk()
		self.C = tk.Canvas(self.top, bg="black", 
			height = self.yd * self.yScaleFactor, 
			width = self.xd * self.xScaleFactor)

		self.next_x_updated = 0
		self.next_y_updated = 0

		self.restartImage = False

		self.pixelsDrawn = 0

		# TODO - remove this after adding rendering in a separate thread
		pixelsToBeDisplayedTest = 250

		while True:
			serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			serversocket.bind((socket.gethostname(), 4000))
			serversocket.listen(5)
			while True:
				connection, client_address = serversocket.accept()
				print('Connected')
				packetsInSecond = 0
				clock = 0
				while True:
					startTime = time.time() 

					IPData =list(connection.recv(36)) #bytearray?s

					clock += time.time() - startTime
					packetsInSecond += 1
					if (clock > 100):
						print('Packets = ')
						print(packetsInSecond)
						packetsInSecond = 0
						clock = 0

					#print("ipdata=", IPData)
					CANData = IPData[-8:]
					#print('can data')
					#print(CANData)

					packet = CANData




								
					#print("packet test")
					#print(packet)


					self.DrawPixels(packet)




t = Test()

t.ProcessCAN()

