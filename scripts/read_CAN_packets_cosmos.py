import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('192.168.1.152', 1235)
print(sys.stderr, 'starting up on %s port %s' % server_address)
sock.connect(server_address)

# Listen for incoming connections
while True:
   # Wait for a connection
   try:
       # Receive the data in small chunks and retransmit it
       while True:
           data = sock.recv(36)
           print(sys.stderr, 'received "%s"' % data)
   finally:
       # Clean up the connection
       connection.close()