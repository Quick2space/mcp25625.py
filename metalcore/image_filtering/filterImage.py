import numpy as np
import colorsys
from PIL import Image

'''

Determine weather to send the current image or not.
@param filename : the name of picture, that needs to be validated
@param intensityThreshold : heuristic on intensity
@counts : heuristic on minimum number of pixels
@returns boolen : True -> send image, False -> Don't send Image
'''
def filterImage(filename, intensityThreshold=0.25,
                counts=5):
    thresholdCrossingCount = 0
    # (1) Import the file to be analyzed!
    img_file = Image.open(filename)
    img = img_file.load()

    # (2) Get image width & height in pixels
    [xs, ys] = img_file.size
    max_intensity = 100
    hues = {}

    # (3) Examine each pixel in the image file
    for x in range(0, xs):
        for y in range(0, ys):
            # (4)  Get the RGB color of the pixel
            [r, g, b] = img[x, y]

            # (5)  Normalize pixel color values
            r /= 255.0
            g /= 255.0
            b /= 255.0

            # (6)  Convert RGB color to HSV
            [h, s, v] = colorsys.rgb_to_hsv(r, g, b)

            # (7)  Marginalize s; count how many pixels have matching (h, v)
            if h not in hues:
                hues[h] = {}
            if v not in hues[h]:
                hues[h][v] = 1
            else:
                if hues[h][v] < max_intensity:
                    hues[h][v] += 1

            # If s is greater then a certain threshold, a "good"
            #  number of times, then raise the flag
            if(v > intensityThreshold):
                thresholdCrossingCount += 1

    if (thresholdCrossingCount > counts):
        return True
    else:
        return False
