#!/usr/bin/env python3
# Copyright (C) 2018 Quick2Space.org under the MIT License (MIT)
# See the LICENSE.txt file in the project root for more information.

import tkinter as tk
import picamera as pc
import picamera.array as pcarray
import time
import numpy 

from time import sleep
from metalcore.MCP25625_api import MCP25625_api, Message

class picture_recv(object):

    def __init__(self):
        # Setup CAN API
        self.can = MCP25625_api() #verbosePrint = True) # - Set this to get more debugging information.

        self.can.SetFilterIdF0(0x12180320)
        self.can.Initialize()
        
        self.can.SetNormalMode() # Uncomment for normal mode.
        # self.can.SetLoopbackMode() # Uncomment for loopback (debug) mode.

        self.xd = 96
        self.yd = 64

        # 5 pixels width 
        self.xScaleFactor = 6
        self.yScaleFactor = 5

        self.lastData = 0

        self.top = tk.Tk()
        self.C = tk.Canvas(self.top, bg="black", 
            height = self.yd * self.yScaleFactor, 
            width = self.xd * self.xScaleFactor)

        self.next_x_updated = 0
        self.next_y_updated = 0

        self.restartImage = False

        self.pixelsDrawn = 0

        # TODO - remove this after adding rendering in a separate thread
        self.pixelsToBeDisplayedTest = 250


    def CalculateChecksum(self, data):
        s = 0
        for d in data:
            s = 7 * s + int(d % 255) * 11
            s = s % 255
        # print(data, " ---> ", s)
        return s

            
    def DrawPixels(self, arr):
        last_x = arr[0]
        last_y = arr[1]
        chksum = arr[7]

        pixels = arr[2:7]
        assert len(pixels) == 5

        received_chksum = self.CalculateChecksum(arr[0:7])

        # Invalid data?
        if chksum != received_chksum:
            return False
        
        x = last_x
        y = last_y

        while len(pixels) > 0:
            grayShade = (int(pixels[0])) % 255

            color = "#%02x%02x%02x" % (grayShade, grayShade, grayShade)
            # print("x y", x, y)
            self.C.create_rectangle(y * self.yScaleFactor, 
                    x * self.xScaleFactor,
                    (y + 1) * self.yScaleFactor, 
                    (x + 1) * self.xScaleFactor, 
                    fill=color)

            self.pixelsDrawn += 1 

            pixels = pixels[1:]
            y += 1
            if (y >= self.yd):
                y = 0
                x += 1
                if (x > self.xd):
                    x = 0

        self.next_x_updated = x
        self.next_y_updated = y

        # Display some partial progress
        self.C.pack()
        self.top.update()

        # TODO - add threading and continuous update
        # if self.pixelsToBeDisplayedTest <= self.pixelsDrawn:
        #    self.top.mainloop()

        return True


    def Read(self):
        try:
            recvMsg = self.can.Recv(timeoutMilliseconds=2000)
            
            if (not self.DrawPixels(recvMsg.data)):
                print("!!! RECV: {0}".format(recvMsg))
            else:
                # print("    RECV: {0}".format(recvMsg))
                pass
            self.lastData = recvMsg.data[0]
            
        except TimeoutError as e:
            print("    RECV: Timeout receiving message. <{0}>".format(e))


def main():
    pr = picture_recv()
    idx = 0
    while(True):
        pr.Read()
        idx += 1
        if (idx % 10) == 0: 
            print("(%d)" % idx)


if __name__ == "__main__":
    main()
