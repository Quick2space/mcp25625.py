#!/usr/bin/env python3
# Copyright (C) 2018 Quick2Space.org under the MIT License (MIT)
# See the LICENSE.txt file in the project root for more information.

import picamera as pc
import picamera.array as pcarray
import time
import numpy 

from time import sleep
from metalcore.MCP25625_api import MCP25625_api, Message


class picture_send(object):

    def __init__(self):

        # Effective data size per packet
        # CAN packet size (8) minus bytes for x, y and chksum
        # changed from 5 to 10 in order to pack two pixels per byte 
        self.channelSize = 10 

        # Setup CAN API
        self.can = MCP25625_api() #verbosePrint = True) # - Set this to get more debugging information.
        self.can.Initialize()
        
        self.can.SetNormalMode() # Uncomment for normal mode.
        # self.can.SetLoopbackMode() # Uncomment for loopback (debug) mode.
            
        self.xd = 96
        self.yd = 64


    def Read(self):
        try:
            recvMsg = self.can.Recv()
            print("    RECV: {0}".format(recvMsg))
        except TimeoutError as e:
            print("    RECV: Timeout receiving message. <{0}>".format(e))

    def CalculateChecksum(self, data):
        s = 0
        for d in data:
            s = 7 * s + int(d % 255) * 11
            s = s % 255
        print(data, " ---> ", s)
        return s


    def SendPixels(self, x, y, pixels):
        # arbitration_id = 0b00000000000000000000000000000 # Extended Mode, 29bit
        # arbitration_id = 0b11111111111111111111111111111 # Extended Mode, 29bit
        # arbitration_id = 0b11101010101010101010101010111 # Extended Mode, 29bit
        arbitration_id = 0x12180320 # Extended Mode, 29bit
        
        assert len(pixels) <= self.channelSize

        # TODO do better compression here (cross-packet relationship!)
        # no more TODO, packing 10 pixels instead of 5 per packet 
        ten_pixel_arr = []
        for p in range(0, self.channelSize, 2):
            pixel1_4bit = int(pixels[p] / 16)
            pixel2_4bit = int(pixels[p+1] / 16)
            pixels_combined = (pixel1_4bit << 4) | pixel2_4bit
            ten_pixel_arr.append(pixels_combined)

        # data = [x, y] + pixels[0:self.channelSize]
        data = [x, y] + ten_pixel_arr
        ten_pixel_arr= []

        # Checksum the first 7 bytes of the CAN data payload
        chksum = self.CalculateChecksum(data)

        # Add the checksum as the last byte
        data += [chksum]

        dataBytes = [int(d) for d in data]

        time.sleep(0.05)

        msg = Message(arbitration_id, dataBytes) #, extended_id=False)


        try:
            self.can.Send(msg, timeoutMilliseconds=1000)
            print("    SEND: {0}".format(msg))
        except TimeoutError as e:
            print("    SEND: Timeout sending message. <{0}>".format(e))


    def SendArray(self, arr):
        xd = arr.shape[0]
        assert xd == self.xd

        yd = arr.shape[1]
        assert yd == self.yd

        pixels = []
        for x in range(xd):
            for y in range(yd):
                if (len(pixels) == 0):
                    last_x = x
                    last_y = y

                grayShade = arr[x, y]

                # Add one more pixel
                pixels += [grayShade]
                
                if (len(pixels) >= self.channelSize):
                    # print(pixels)
                    self.SendPixels(last_x, last_y, pixels)
                    pixels = []

        # send remaining pixels. Pad with zeros if needed
        if (len(pixels) > 0):
            pixels += [0] * (10 - len(pixels)) #changed from 5 to 10 bc we are sending more pixels now
            self.SendPixels(last_x, last_y, pixels)



    def CaptureArray(self):
        with pc.PiCamera() as camera:
            rx = self.xd
            ry = self.yd
            camera.resolution = (ry, rx)
            # camera.start_preview()
            # time.sleep(2)
            # camera.stop_preview()
            with pcarray.PiRGBArray(camera) as output:
                camera.capture(output, 'rgb')
                xd = output.array.shape[0]
                yd = output.array.shape[1]
                cd = output.array.shape[2]

                assert xd == self.xd
                assert yd == self.yd

                print('Captured %dx%d image (%d color), size = %.2f KB w. color' % (
                        yd, 
                        xd,
                        cd,
                        xd * yd * cd / 1024
                        ))
                # print("Data: ", output.array)

                oa = output.array 
                grayImage = numpy.zeros(shape = (xd, yd), dtype = numpy.uint8)
                for x in range(xd):
                    for y in range(yd):
                        # TODO - add better RGB weighting? 
                        grayColor = int((float(oa[x,y,0]) + 
                            float(oa[x,y,1]) + 
                            float(oa[x,y,2]))/3)
                        grayImage[x][y] = grayColor
                        #  print(x, y, " ->", (oa[x,y,0], oa[x,y,1], oa[x,y,2]), grayColor)

                # print("----------- Gray vector: ")
                # print(grayImage)
                return grayImage



def main():
    ps = picture_send()
    while True:
        image = ps.CaptureArray()
        ps.SendArray(image)

if __name__ == "__main__":
    main()



