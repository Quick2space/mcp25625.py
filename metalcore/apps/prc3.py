#!/usr/bin/env python3
# Copyright (C) 2018 Quick2Space.org under the MIT License (MIT)
# See the LICENSE.txt file in the project root for more information.

import tkinter as tk
# import picamera as pc
# import picamera.array as pcarray
import time
import numpy 

from time import sleep
# from MCP25625_api import MCP25625_api, Message

import socket
import sys

def filterCameraCanId(arr):
    a2 = arr[24:28]
    # print(len(a2))
    return (a2 == bytearray([0x12,0x18,0x03,0x20]))
    #print(a2)
    #return (a2 == [0x12,0x69, 0x02, 0x5f])


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('192.168.1.152', 1235)
print(sys.stderr, 'starting up on %s port %s' % server_address)
sock.connect(server_address)


class picture_recv(object):

    def __init__(self):
        # Setup CAN API
        #self.can = MCP25625_api() #verbosePrint = True) # - Set this to get more debugging information.

        #self.can.SetFilterIdF0(0x12180320)
        #self.can.Initialize()
        
        #self.can.SetNormalMode() # Uncomment for normal mode.
        # self.can.SetLoopbackMode() # Uncomment for loopback (debug) mode.

        self.xd = 96
        self.yd = 64

        # 5 pixels width 
        self.xScaleFactor = 6
        self.yScaleFactor = 5

        self.lastData = 0

        self.top = tk.Tk()
        self.C = tk.Canvas(self.top, bg="black", 
            height = self.yd * self.yScaleFactor, 
            width = self.xd * self.xScaleFactor)

        self.next_x_updated = 0
        self.next_y_updated = 0

        self.restartImage = False

        self.pixelsDrawn = 0

        # TODO - remove this after adding rendering in a separate thread
        self.pixelsToBeDisplayedTest = 250


    def CalculateChecksum(self, data):
        s = 0
        for d in data:
            s = 7 * s + int(d % 255) * 11
            s = s % 255
        # print(data, " ---> ", s)
        return s

            
    def DrawPixels(self, arr):
        last_x = arr[0]
        last_y = arr[1]
        chksum = arr[7]

        pixels = arr[2:7]
        assert len(pixels) == 5

        received_chksum = self.CalculateChecksum(arr[0:7])

        # Invalid data?
        if chksum != received_chksum:
            return False
        
        x = last_x
        y = last_y

        unpacked_pixels = []
        for pixel in pixels:
            pixel_binary = "{0:08b}".format(pixel)
            pixel1 = int(pixel_binary[:4], 2)
            pixel2 = int(pixel_binary[4:], 2)
            unpacked_pixels.append(pixel1 * 16)
            unpacked_pixels.append(pixel2 * 16)

        assert len(unpacked_pixels) == 10
        # print(unpacked_pixels)
        while len(unpacked_pixels) > 0:
            grayShade = (int(unpacked_pixels[0])) % 255 
            
            color = "#%02x%02x%02x" % (grayShade, grayShade, grayShade)
            # print("x y", x, y)
            self.C.create_rectangle(y * self.yScaleFactor, 
                    x * self.xScaleFactor,
                    (y + 1) * self.yScaleFactor, 
                    (x + 1) * self.xScaleFactor, 
                    fill=color)

            self.pixelsDrawn += 1 

            unpacked_pixels = unpacked_pixels[1:]
            y += 1
            if (y >= self.yd):
                y = 0
                x += 1
                if (x > self.xd):
                    x = 0

        self.next_x_updated = x
        self.next_y_updated = y

        # Display some partial progress
        self.C.pack()
        self.top.update()

        # TODO - add threading and continuous update
        # if self.pixelsToBeDisplayedTest <= self.pixelsDrawn:
        #    self.top.mainloop()

        return True


    def Read(self):
        try:
            # recvMsg = self.can.Recv(timeoutMilliseconds=2000)

            data = sock.recv(36)
            print('-----------')
            d = bytearray(data)
            if (not filterCameraCanId(d)):
               # continue
               print("not match")
               pass
            else:
               print("match")

            s = '> '
            for (i,x) in enumerate(d):
               s += ',' + str(i) + ':' + hex(x)
               if (i % 8 == 0) and (i > 0):
                   s += '\n'

            print(sys.stderr, 'received "%s"' % s)

            candata=d[28:]
            
            if (not self.DrawPixels(candata)):
                print("!!!error:{0}" % data)
                return False
            else:
                # print("    RECV: {0}".format(recvMsg))
                pass
            self.lastData = candata
            
        except TimeoutError as e:
            print("    RECV: Timeout receiving message. <{0}>".format(e))


if __name__ == "__main__":
    pr = picture_recv()
    idx = 0
    try:
      while(True):
        pr.Read()
        idx += 1
        if (idx % 10) == 0: 
            print("(%d)" % idx)
    finally:
      connection.close()
